var app = new Vue({
    el: '#app',
    data: {
          toilets: []
    },
    created() {
      fetch('https://s3-us-west-2.amazonaws.com/s.cdpn.io/194554/mijnfile.json')
        .then(response => response.json())
        .then(json => {
            this.toilets = json.blogposts
       })
    }
})

/*

Jij zal dit doen met lokale JSON-bestanden. Dat betekent dat fetch een lokaal json bestand zal moeten inladen:

fetch("openbaar_toilet.geojson")
  .then(response => response.json())
  .then(json => {
      this.posts = json.blogposts
 })

 fetch('https://s3-us-west-2.amazonaws.com/s.cdpn.io/194554/mijnfile.json')
   .then(response => response.json())
   .then(json => {
       this.posts = json.blogposts
 })

Je zal het eindresultaat dan moeten bekijken vanop een web server:

Je kan daarvoor best een eenvoudige lokale webserver gebruiken (via de terminal): https://www.npmjs.com/package/http-server


*/
